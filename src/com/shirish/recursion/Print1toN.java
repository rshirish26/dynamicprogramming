package com.shirish.recursion;

public class Print1toN {

	public static void main(String[] args) {
		int n = 7;
	//	print 1 to 7
        print(7);
	}
	
	public static void print(int n)
	{
		if(n ==0)
			return;
		print(n-1);
		System.out.print(n +" ");
	}

}
