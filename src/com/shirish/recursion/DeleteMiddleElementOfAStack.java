package com.shirish.recursion;

import java.util.Arrays;
import java.util.Stack;

public class DeleteMiddleElementOfAStack {

	public static void main(String[] args) {
		Stack<Integer> stack = new Stack<Integer>();
		stack.add(1);
		stack.add(2);
		stack.add(3);
		stack.add(4);
		stack.add(5);
		int middle=	(stack.size()/2)+1;
		Stack<Integer> result  =deleteMiddleElement(stack, middle);
	    
		System.out.println(Arrays.toString(result.toArray()));

	}

	public static Stack<Integer> deleteMiddleElement(Stack<Integer> stack, int middle){
		if(middle == 1) {
			stack.pop();
			return stack;
		}
		
		
		
		
		int temp = stack.pop();
		deleteMiddleElement(stack, middle-1);
		stack.push(temp);
		return stack;
		
	}

}
