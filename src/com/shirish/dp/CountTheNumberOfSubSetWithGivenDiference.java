package com.shirish.dp;

public class CountTheNumberOfSubSetWithGivenDiference {

	public static void main(String[] args) {
		int [] nums = {1, 1, 2, 3};
        int diff = 1;
        
        int range = 0;
        for(int i =0; i< nums.length; i++)
        {
        	range+= nums[i];
        }
        
        System.out.println("Count of the subsets with given difference 1 :   "+subsetSumProblem(nums, range, diff));
	}
	
	public static int subsetSumProblem(int [] nums, int k, int diff)
	{
		//initialize
		
		System.out.println("Range : "+k);
		int [][] t = new int [nums.length+1][k+1];
		 for(int i =0; i<= nums.length; i++){
	            for(int j = 0; j<= k; j++)
	            {
	            	if(i==0)
	                {
	                   t[i][j] = 0;        
	                }
	                if(j==0)
	                {
	                    t[i][j] = 1;
	                }
	            }
	        }
		
		for(int i=1 ; i<= nums.length; i++)
		{
			for(int j=1; j<=k; j++)
			{
				if(nums[i-1] <= j)
				{
				t[i][j] = (t[i-1][j-nums[i-1]]) + t[i-1][j]	;
				}
				else 
				{
					t[i][j] = t[i-1][j]	;
				}
			}
		}
		
		int count = 0;
		
		for(int i=0 ; i<= nums.length; i++)
		{
			System.out.println();
			for(int j=0; j<=k; j++)
			{
				System.out.print(t[i][j] + "  ");
			}
			System.out.println();	
		}
		
		
		for(int j=0; j<=k; j++)
		{
			if((2*j)-(k) == diff)
			{
				System.out.println("J ka value +" +j);
             return t[nums.length][j];
			
			}
		}
		
		
		return 0;
		
	}

}
