package com.shirish.dp.lcs.variation;

public class ShortestCommonSuperSequence {

	public static void main(String[] args) {
		String input1 = "geek";
		String input2 = "eke";
		String output = "geeke";

		int[][] t = new int[input1.length() + 1][input2.length() + 1];

		for (int i = 0; i <= input1.length(); i++) {
			for (int j = 0; j <= input2.length(); j++) {
				if (i == 0 || j == 0)
					t[i][j] = 0;
			}
		}

		for (int i = 1; i <= input1.length(); i++) {
			for (int j = 1; j <= input2.length(); j++) {
				if (input1.charAt(i - 1) == input2.charAt(j - 1)) {
					t[i][j] = 1 + t[i - 1][j - 1];
				} else {
					t[i][j] = Math.max(t[i - 1][j], t[i][j - 1]);
				}
			}
		}

		/*
		 * for (int i = 0; i <= input1.length(); i++) { System.out.println(); for (int j
		 * = 0; j <= input2.length(); j++) { System.out.print(t[i][j] + " "); } }
		 */

		int i = input1.length();
		int j = input2.length();
		StringBuffer result = new StringBuffer();
		result.append("");
		while (i > 0 && j > 0) {
			if (input1.charAt(i - 1) == input2.charAt(j - 1)) {
				result.append(input2.charAt(j - 1));
				i--;
				j--;

			} else {
				if (t[i - 1][j] > t[i][j - 1]) {
					result.append(input1.charAt(i - 1));
					i--;
				} else {
					result.append(input2.charAt(j - 1));
					j--;

				}

			}
		}

		result.reverse();
		if (i > 0) {

			result.insert(0, input1.substring(0, i));
		}
		if (j > 0) {

			result.insert(0, input2.substring(0, j));
		}

		System.out.println("Longest Common Subsequence (6) " + result);
	}

}
