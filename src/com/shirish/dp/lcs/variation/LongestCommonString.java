package com.shirish.dp.lcs.variation;

public class LongestCommonString {

	public static void main(String[] args) {
//		String input1 = "abcde";
//		String input2 = "abfce";
		String input1 = "GeeksforGeeks";
		String input2 = "GeeksQuiz";
		
		int output = 5;
		
		System.out.println("Longest Common  SubString::::::" + longestCommonSubstring(input1, input2));

	}

	private static int longestCommonSubstring(String input1, String input2) {
		int[][] t = new int[input1.length() + 1][input2.length() + 1];

		for (int i = 0; i <= input1.length(); i++) {
			for (int j = 0; j <= input2.length(); j++) {
				if (i == 0 || j == 0)
					t[i][j] = 0;
			}
		}
		int max = Integer.MIN_VALUE;
		for (int i = 1; i <= input1.length(); i++) {
			for (int j = 1; j <= input2.length(); j++) {
				if (input1.charAt(i - 1) == input2.charAt(j - 1)) {
					t[i][j] = 1 + t[i - 1][j - 1];
					max = Math.max(max,t[i][j]);
				} else {
					t[i][j] = 0;
				}
			}
		}
		
		for (int i = 0; i <= input1.length(); i++) {
			System.out.println();
			for (int j = 0; j <= input2.length(); j++) {
					System.out.print(t[i][j] + " ");
			}
		}
			
		return max;
	}

}
