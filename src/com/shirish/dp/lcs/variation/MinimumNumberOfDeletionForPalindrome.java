package com.shirish.dp.lcs.variation;

/*Minimum number of deletions to make a string palindrome
Given a string of size ‘n’. The task is to remove or delete minimum number of characters from the string so that the resultant string is palindrome.
Examples :

Input : aebcbda
Output : 2
Remove characters 'e' and 'd'
Resultant string will be 'abcba'
which is a palindromic string*/

public class MinimumNumberOfDeletionForPalindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		 String input = "aebcbda";
		 String input1 = "aebcbda";
		              //    adbcbea
		 String input2 = new StringBuffer("aebcbda").reverse().toString();
			int[][] t = new int[input1.length() + 1][input2.length() + 1];

			for (int i = 0; i <= input1.length(); i++) {
				for (int j = 0; j <= input2.length(); j++) {
					if (i == 0 || j == 0)
						t[i][j] = 0;
				}
			}

			for (int i = 1; i <= input1.length(); i++) {
				for (int j = 1; j <= input2.length(); j++) {
					if (input1.charAt(i - 1) == input2.charAt(j - 1)) {
						t[i][j] = 1 + t[i - 1][j - 1];
					} else {
						t[i][j] = Math.max(t[i - 1][j], t[i][j - 1]);
					}
				}
			}
			
			for (int i = 0; i <= input1.length(); i++) {
				System.out.println();
				for (int j = 0; j <= input2.length(); j++) {
						System.out.print(t[i][j] + " ");
				}
			}
			System.out.println("Minimum number of deletions to make a string palindrome" +(input.length()- t[input1.length()][input2.length()]));
		 

	}

}
