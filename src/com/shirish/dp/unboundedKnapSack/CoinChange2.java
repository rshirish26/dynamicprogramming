package com.shirish.dp.unboundedKnapSack;

public class CoinChange2 {

	public static void main(String[] args) {
		int coins [] = {25, 10, 5};
		int sum= 30;
		//Minimum number of coins required to get the sum as 30;
		int minNumberOfCoins = minimumNumberCoins(coins, sum);
		System.out.println("Result : " +minNumberOfCoins);


	}
	
	
	public static int minimumNumberCoins(int [] nums, int k)
	{
		int[][] t = new int[nums.length + 1][k + 1];

		// baseConditionn
		for (int i = 0; i <= nums.length; i++) {
			for (int j = 0; j <= k; j++) {
			/*	if(i==1 && j>=1)
				{
					t[i][j] = (j % nums[i] == 0) ? j / nums[i] : Integer.MAX_VALUE-1 ;
				}*/
				if (i == 0 ) {
					t[i][j] = Integer.MAX_VALUE-1;
				}
				if (j == 0)
				{
					t[i][j] = 0;
				}
				
				
			}
		}
		for(int j=1; j<=k; j++)
		{
			t[1][j] = (j % nums[0] == 0) ? j / nums[0] : Integer.MAX_VALUE-1 ;
		}
		
		for(int i=0 ; i<= nums.length; i++)
		{
			System.out.println();
			for(int j=0; j<=k; j++)
			{
				System.out.print(t[i][j] + "  ");
			}
		}
		
		//code 
		for (int i = 2; i <= nums.length; i++) {
			for (int j = 1; j <= k; j++) {

				if (nums[i - 1] <= j) {
				//	System.out.print("->  "+t[i][j - nums[i - 1]] + "     ");
				//	System.out.println("->  "+t[i - 1][j]);
					t[i][j] =Math.min(t[i][j - nums[i - 1]] +1, t[i - 1][j]);
				} else {
					t[i][j] = t[i - 1][j];
				}

			}
		}
		
		for(int i=0 ; i<= nums.length; i++)
		{
			System.out.println();
			for(int j=0; j<=k; j++)
			{
				System.out.print(t[i][j] + "  ");
			}
		}
		return t[nums.length][k];
		
	}

}
