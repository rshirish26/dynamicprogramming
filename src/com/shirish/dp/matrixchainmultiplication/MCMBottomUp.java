package com.shirish.dp.matrixchainmultiplication;

import java.util.Arrays;

public class MCMBottomUp {

	public static int[][] t = new int[1001][1001];

	public static void main(String[] args) {
		for (int[] row: t)
		    Arrays.fill(row,  -1);

		int[] inputArray = { 40, 20, 30, 10, 30 };
		int i = 1;
		int j = inputArray.length - 1;

		int result = mcmBottonUp(inputArray, i, j);
		System.out.println("Result is here : " + result);

	}

	private static int mcmBottonUp(int[] inputArray, int i, int j) {

		if(i>=j)
			return 0;
		
		if (t[i][j] != -1)
			return t[i][j];

		int minimum = Integer.MAX_VALUE;
		for (int k = i; k <= j - 1; k++) {
			int temp = mcmBottonUp(inputArray, i, k) + mcmBottonUp(inputArray, k + 1, j)
					+ (inputArray[i - 1] * inputArray[k] * inputArray[j]);
			minimum = Math.min(minimum, temp);
		}
		t[i][j] = minimum;
		return t[i][j];
	}

}
